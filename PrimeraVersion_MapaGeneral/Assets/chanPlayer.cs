﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chanPlayer : MonoBehaviour {

    public float speed = 10;
    public float moveSpeed;
    public CharacterController player;
    private Vector3 movement;
	// Use this for initialization
	void Start () {
        player = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        movement.z = 1 * moveSpeed * Time.deltaTime;
        movement.x = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed;
        movement.y = 0;
        player.Move(movement);
	}
}
